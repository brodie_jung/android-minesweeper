package ca.cmpt276.as3.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;
import android.widget.PopupWindow;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Objects;

import ca.cmpt276.as3.R;

public class VictoryActivity extends Activity {

    protected void onCreate(Bundle savedInstanceState){
        //Sets theme to the custom popup theme
        setTheme(R.style.AppThemePopup);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_victory);
        //Sets all the attributes so that this activity will be a popup screen that 80% the size of height and width
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.8),(int)(height*0.8));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity  = Gravity.CENTER;
        params.x = 0;
        params.y = -20;

        getWindow().setAttributes(params);
    }
    //Overrides back button so that it goes to main activity
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(VictoryActivity.this,MainActivity.class));
        finish();
    }
    //Gets intent
    public static Intent makeIntent(Context context){
        return new Intent(context, VictoryActivity.class);
    }
}
