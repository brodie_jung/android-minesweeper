package ca.cmpt276.as3.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioAttributes;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import ca.cmpt276.as3.R;
import ca.cmpt276.as3.model.Game;
import ca.cmpt276.as3.model.Tile;

public class GameActivity extends AppCompatActivity {

    private static final String PREFS_NAME = "AppPrefs";

    private static final Integer mineImage = R.drawable.ic_baseline_mine;
    private static final Integer openMineImage = R.drawable.ic_baseline_open_mine;
    private static final Integer hiddenImage = R.drawable.ic_baseline_hidden;

    private SoundPool soundPool;
    private int mineSound, scanSound;

    public static Intent makeIntent(Context context) {
        return new Intent(context, GameActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // Initialize Sound Pool
        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        soundPool = new SoundPool.Builder()
                .setMaxStreams(2)
                .setAudioAttributes(audioAttributes)
                .build();
        mineSound = soundPool.load(this, R.raw.normal_ping, 1);
        scanSound = soundPool.load(this, R.raw.sonar_ping, 1);
    }

    private void updateInfo() {
        Game game = Game.getInstance();

        TextView mineCountInfo = findViewById(R.id.game_mineCountInfo);
        String mineCountText = "Found Mines: " + game.getFoundMines() + "/" + game.getTotalMines();
        mineCountInfo.setText(mineCountText);

        int highScore;
        if (!game.isResetFlag()) {
            highScore = getHighScore(GameActivity.this);
            Game.getInstance().setHighScore(highScore);
        }
        TextView highScoreInfo = findViewById(R.id.game_highScoreInfo);
        String highScoreText;
        if (game.getHighScore() == 99999) highScoreText = "High score out of " + game.getGamesPlayed() + " games: N/A";
        else highScoreText = "High score out of " + game.getGamesPlayed() + " games: " + game.getHighScore() + " scans used";
        highScoreInfo.setText(highScoreText);

        TextView scansInfo = findViewById(R.id.game_scanInfo);
        String scansText = "Scans Used: " + game.getScansUsed();
        scansInfo.setText(scansText);
    }

    private void setupGrid() {
        final GridView grid = findViewById(R.id.game_field);
        grid.setAdapter(new GridAdapter());

        final Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        final long[] patternMine = {0, 1000};
        final long[] patternScan = {2000, 4000};

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Game game = Game.getInstance();
                int x = position % game.getBoardWidth();
                int y = position / game.getBoardWidth();
                ImageView icon = view.findViewById(R.id.game_grid_icon);
                TextView text = view.findViewById(R.id.game_grid_text);

                // Case: New Mine Discovered
                if (game.reveal(x, y)) {
                    vibrator.vibrate(patternMine,0);
                    soundPool.play(mineSound,1,1,0,0,1);
                    icon.setImageResource(mineImage);
                    // Update other denominations
                    for (int yC=0; yC<game.getBoardHeight(); yC++) {
                        for (int xC=0; xC<game.getBoardWidth(); xC++) {
                            Tile tile = game.getTile(xC, yC);
                            View tileView = grid.getChildAt(twoDToOneD(xC, yC));
                            TextView denomination = null;
                            if (tileView!=null) denomination = tileView.findViewById(R.id.game_grid_text);
                            if (tile.getDenomination() != null && denomination != null) {
                                game.reveal(xC, yC);
                                denomination.setText(String.valueOf(tile.getDenomination()));
                            }
                        }
                    }
                }
                // Case: Scan
                else {
                    // Vibrate
                    vibrator.vibrate(patternScan,0);
                    // Play Sound
                    soundPool.play(scanSound,1,1,0,0,1);

                    Tile tile = game.getTile(x, y);
                    if (tile.isMined()) icon.setImageResource(openMineImage);
                    text.setText(String.valueOf(tile.getDenomination()));
                }

                // Check win condition and activate victory screen
                if (game.hasWon()) saveHighScore(game.getHighScore());
                //Launch victory screen if won
                if(game.hasWon()){
                    Intent intent = new Intent(getApplicationContext(),VictoryActivity.class);
                    startActivity(intent);
                    //Resets the game board after victory
                    game.initializeBoard(OptionsActivity.getBoardWidth(GameActivity.this),OptionsActivity.getBoardHeight(GameActivity.this),OptionsActivity.getNumMines(GameActivity.this),game.getGamesPlayed());
                }

                // Update top counters
                updateInfo();
            }
        });
        grid.setNumColumns(Game.getInstance().getBoardWidth());

    }

    private int twoDToOneD(int x, int y) {
        return x + (y * Game.getInstance().getBoardWidth());
    }

    private class GridAdapter extends BaseAdapter {

        public GridAdapter() {
            super();
        }

        @Override
        public int getCount() {
            return Game.getInstance().getBoardSize();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.grid_view_game_tile, parent, false);
            }

            // Initialize icon and text
            ImageView icon = view.findViewById(R.id.game_grid_icon);
            TextView text = view.findViewById(R.id.game_grid_text);
            icon.setImageResource(hiddenImage);
            text.setText("");

            return view;
        }
    }

    private void saveHighScore(int highScore) {
        SharedPreferences numHighScore = this.getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        SharedPreferences.Editor editor = numHighScore.edit();
        editor.putString("High Score",String.valueOf(highScore));
        editor.apply();
    }

    static public int getHighScore(Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return Integer.parseInt(Objects.requireNonNull(prefs.getString("High Score", "99999")));
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Setup Default Game
        Game game = Game.getInstance();
        if (game.getBoardSize() == 0) game.initializeBoard(OptionsActivity.getBoardWidth(this),OptionsActivity.getBoardHeight(this),OptionsActivity.getNumMines(this),game.getGamesPlayed());

        // Update Elements
        updateInfo();
        setupGrid();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        soundPool.release();
        soundPool = null;
    }


}