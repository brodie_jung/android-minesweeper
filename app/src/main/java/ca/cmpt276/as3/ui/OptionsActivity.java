package ca.cmpt276.as3.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import ca.cmpt276.as3.R;
import ca.cmpt276.as3.model.Game;

public class OptionsActivity extends AppCompatActivity {
    private static final String PREFS_NAME = "AppPrefs";
    //Gets intent
    public static Intent makeIntent(Context context){
        return new Intent(context, OptionsActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        //Sets up radio buttons
        createBoardSize();
        createNumMines();
        resetGamesPlayedText();
        resetButton();
    }

    //Resets the values of the highscore and number of games played using singleton
    private void resetButton(){
        Button btn = findViewById(R.id.reset);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetGamesPlayed(OptionsActivity.this);
                resetGamesPlayedText();
                Game.getInstance().setHighScore(99999);
                Game.getInstance().setResetFlag(true);
            }
        });
    }

    private void resetNumber(){
        int gamesPlayed = getGamesPlayed(this);
        TextView tvGamesPlayed = findViewById(R.id.games_played);
        tvGamesPlayed.setText(getString(R.string.games_played, gamesPlayed));
    }

    //Updates the games played in sharedprefernces so it is consistent even if app is closed
    private void resetGamesPlayed(Context context){
        SharedPreferences gamesPlayedPref = context.getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        SharedPreferences.Editor editor = gamesPlayedPref.edit();
        editor.putInt("Games Played",0);
        editor.apply();
    }

    //Resets the text when the button is pressed
    private void resetGamesPlayedText(){
        int gamesPlayed = getGamesPlayed(this);
        TextView tvGamesPlayed = findViewById(R.id.games_played);
        tvGamesPlayed.setText(getString(R.string.games_played, gamesPlayed));
    }

    //Whenever the play game button is pressed in the main menu
    //Increment the number of games played in shared preferences by 1
    static public void incrementGamesPlayed(Context context) {
        Game.getInstance().setGamesPlayed(Game.getInstance().getGamesPlayed() + 1);
        SharedPreferences gamesPlayedPref = context.getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        SharedPreferences.Editor editor = gamesPlayedPref.edit();
        int newPlayed = getGamesPlayed(context) + 1;
        editor.putInt("Games Played",newPlayed);
        editor.apply();
    }


    static public int getGamesPlayed(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return prefs.getInt("Games Played", 0);
    }

    private void createNumMines() {
        RadioGroup group = findViewById(R.id.numMines);
        int[] numMines = getResources().getIntArray(R.array.num_mines);
        for (final int numMine : numMines) {
            RadioButton button = new RadioButton(this);
            button.setText(getString(R.string.mines, numMine));

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    saveNumMines(numMine);
                }
            });

            group.addView(button);

            if (numMine == getNumMines(this)) {
                button.setChecked(true);
            }
        }
    }

    private void saveNumMines(int numMine) {
        SharedPreferences numMinePref = this.getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        SharedPreferences.Editor editor = numMinePref.edit();
        editor.putInt("Num Mines",numMine);
        editor.apply();
    }

    static public int getNumMines(Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        int defaultMines = context.getResources().getInteger(R.integer.defaultNumMines);
        return prefs.getInt("Num Mines", defaultMines);
    }

    private void createBoardSize() {
        RadioGroup group = findViewById(R.id.boardSize);
        String[] boardSizes = getResources().getStringArray(R.array.board_size);
        for (final String boardSize : boardSizes) {
            RadioButton button = new RadioButton(this);
            button.setText(getString(R.string.board, boardSize));

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    saveBoardSize(boardSize);
                    int width = getBoardWidth(OptionsActivity.this);
                    int height = getBoardHeight(OptionsActivity.this);
                    int mineCount = getNumMines(OptionsActivity.this);
                    int gamesPlayed = getGamesPlayed(OptionsActivity.this);
                    Game.getInstance().initializeBoard(width, height, mineCount, gamesPlayed);
                }
            });

            group.addView(button);

            if (boardSize.equals(getBoardSize(this))) {
                button.setChecked(true);
            }
        }
    }

    private void saveBoardSize(String boardSize) {
        SharedPreferences numBoardSize = this.getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        SharedPreferences.Editor editor = numBoardSize.edit();
        editor.putString("Board Size",boardSize);
        editor.apply();
    }

    static public String getBoardSize(Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        String defaultSize = context.getResources().getString(R.string.defaultBoardSize);
        return prefs.getString("Board Size", defaultSize);
    }

    static int getBoardHeight(Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        String size =  prefs.getString("Board Size", context.getResources().getString(R.string.defaultBoardSize));
        assert size != null;
        String[] parts = size.split("x");
        return Integer.parseInt(parts[0]);
    }

    static int getBoardWidth(Context context){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        String size =  prefs.getString("Board Size", context.getResources().getString(R.string.defaultBoardSize));
        assert size != null;
        String[] parts = size.split("x");
        return Integer.parseInt(parts[1]);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Game.getInstance().setGamesPlayed(getGamesPlayed(OptionsActivity.this));
    }
}
