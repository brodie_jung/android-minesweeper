package ca.cmpt276.as3.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import ca.cmpt276.as3.R;

public class HelpActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        setupCopyrightLink();
    }

    //Sets up the copyright link so that it actually is a link
    public void setupCopyrightLink(){
        TextView tvCopyright = findViewById(R.id.help_copyright);
        tvCopyright.setMovementMethod(LinkMovementMethod.getInstance());
    }
    //Gets intent
    public static Intent makeIntent(Context context){
        return new Intent(context, HelpActivity.class);
    }
}
