package ca.cmpt276.as3.ui;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Path;
import android.os.Bundle;

import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Objects;

import ca.cmpt276.as3.R;

public class SplashActivity extends MainActivity {
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);

        //Auto skips splash screen after 4 seconds and goes to main
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Objects.requireNonNull(getSupportActionBar()).hide();
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        },4000);
        skipButton(handler);

        ImageView logo = findViewById(R.id.animation);

        //Sets the animation to start from the top then slide down
        Path path = new Path();
        path.arcTo(0f,0f,0f,200f,270f,-180f,true);
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(logo,View.X,View.Y,path);
        animation2.setDuration(2000);
        animation2.start();
    }

    //Sets up the listener for the skip button
    private void skipButton(final Handler handler) {
        Button btn = findViewById(R.id.skipSplash);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SplashActivity.this,MainActivity.class);
                startActivity(intent);
                //If the button is pressed cancel the timer from earlier that auto skips
                handler.removeCallbacksAndMessages(null);
                finish();
            }
        });
    }
    //Gets intent
    public static Intent makeIntent(Context context){
        return new Intent(context, MainActivity.class);
    }
}
