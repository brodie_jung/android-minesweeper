package ca.cmpt276.as3.model;

/**
 * Constituent element of the game board, hosts mines
 */
public class Tile implements Cloneable {

    boolean hidden, mined, scanned;
    Integer denomination;

    public Tile() {
        hidden = true;
        mined = false;
    }

    // Getters and Setters
    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public boolean isMined() {
        return mined;
    }

    public void setMined(boolean mined) {
        this.mined = mined;
    }

    public boolean isScanned() {
        return scanned;
    }

    public void setScanned(boolean scanned) {
        this.scanned = scanned;
    }

    public Integer getDenomination() {
        return denomination;
    }

    public void setDenomination(Integer denomination) {
        this.denomination = denomination;
    }
}
