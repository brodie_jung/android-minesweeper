package ca.cmpt276.as3.model;

import java.util.Random;

/**
 * Sole interface for all game logic, and container object for all game data
 */
public class Game {

    // Critical Components
    private static Game instance;
    private Tile[][] board;

    // Game Data
    private int totalMines, foundMines, scansUsed;
    private int boardWidth, boardHeight, boardSize;
    private int gamesPlayed, highScore;
    private boolean hasWon, resetFlag;

    // Constructor and getInstance
    private Game() {
        totalMines = 0;
        foundMines = 0;
        scansUsed = 0;
        gamesPlayed = 0;
        highScore = 99999;
    }

    /**
     * getInstance methods for the singleton pattern
     * @return the current instance of Game, if no instance exists, it will create a new instance
     */
    public static Game getInstance() {
        if (instance == null) instance = new Game();
        return instance;
    }

    // Initialization Methods
    /**
     * Creates an empty game board of desired size, then place mines
     * @param width width of board
     * @param height height of board
     */
    public void initializeBoard(int width, int height, int mineCount, int gamesPlayed) {
        if (board != null) board = null;

        this.gamesPlayed = gamesPlayed;
        this.hasWon = false;
        this.totalMines = mineCount;
        this.foundMines = 0;
        this.scansUsed = 0;

        board = new Tile[height][width];
        boardWidth = width;
        boardHeight = height;
        boardSize = width * height;
        for (Tile[] tiles : board) {
            for (int i = 0; i < tiles.length; i++) {
                tiles[i] = new Tile();
            }
        }
        setMines(mineCount);
    }

    /**
     * Set randomly placed mines all over the game board
     * @param mineCount amount of mines to be placed
     */
    private void setMines(int mineCount) {
        int capacity = board.length * board[0].length;
        while (mineCount != 0 && capacity != 0) {
            Random rand = new Random();
            int y = rand.nextInt(board.length);
            int x = rand.nextInt(board[0].length);
            if (!board[y][x].isMined()) {
                board[y][x].setMined(true);
                mineCount--;
                capacity--;
            }
        }
    }

    /**
     * Reveals content of tile at (x, y), returns true if there is a hidden mine,
     * returns false and perform scan if there isn't a mine or mine isn't hidden
     * @param x x coordinate of the tile
     * @param y y coordinate of the tile
     */
    public boolean reveal(int x, int y) {

        Tile tile = board[y][x];

        // Hidden mine is found
        if (tile.isHidden() && tile.isMined()) {
            tile.setHidden(false);
            foundMines++;
            if (foundMines == totalMines) win();
            return true;
        }

        // Perform scan
        tile.setHidden(false);
        int count = 0;
        if (!tile.isScanned()) scansUsed++;
        for (int i = 0; i < board[y].length; i++) {
            if (board[y][i].isMined() && board[y][i].isHidden()) count++;
        }
        for (Tile[] tiles : board) {
            if (tiles[x].isMined() && tiles[x].isHidden()) count++;
        }
        tile.setScanned(true);
        tile.setDenomination(count);

        return false;
    }

    /**
     * Get tile at position (x, y)
     * @param x x coordinate of the tile
     * @param y y coordinate of the tile
     * @return the tile
     */
    public Tile getTile(int x, int y) {
        if (board == null) throw new IllegalStateException("Board not initialized!");
        return board[y][x];
    }

    public void win() {
        System.out.println("WIN!");
        if (scansUsed <= highScore) highScore = scansUsed;
        hasWon = true;
    }

    // Getters
    public int getBoardWidth() {
        return boardWidth;
    }

    public int getBoardHeight() {
        return boardHeight;
    }

    public int getBoardSize() {
        return boardSize;
    }

    public int getTotalMines() {
        return totalMines;
    }

    public int getFoundMines() {
        return foundMines;
    }

    public int getScansUsed() {
        return scansUsed;
    }

    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public int getHighScore() {
        return highScore;
    }

    public boolean isResetFlag() {
        return resetFlag;
    }

    public boolean hasWon() {
        return hasWon;
    }

    // Setters for synchronization across activities
    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public void setHighScore(int highScore) {
        this.highScore = highScore;
    }

    public void setResetFlag(boolean resetFlag) {
        this.resetFlag = resetFlag;
    }

}
