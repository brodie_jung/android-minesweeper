package ca.cmpt276.as3.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    @Test
    void getInstance() {
        assertNotNull(Game.getInstance());
    }

    @Test
    void initializeBoard() {
        Game game = Game.getInstance();
        int width = 9, height = 9, count = 0;
        game.initializeBoard(width,height,10);
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (game.getTile(x, y).isMined()) count++;
            }
        }
        assertEquals(10, count);
    }

}